import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';

import { Container } from '@components/common/Container';
import { Block } from '@components/ui/Block';

import { RouteNames } from '@constants/routeNames';
import { Text, TextEnum } from 'components/ui/Text';

type BlockType = {
  name: string
  navigateName: any
  description: string
}

const list: BlockType[] = [
  {
    name: 'Spinner1',
    navigateName: RouteNames.spinner1,
    description: 'Нарастающая дуга по кругу',
  },
];

const Spinners = ({ navigation }: any) => {
  const onPress = (item: BlockType) => navigation.navigate(item.navigateName);

  return (
    <Container>
      <View style={styles.title}>
        <Text type={TextEnum.title}>Spinners</Text>
      </View>
      <ScrollView style={styles.body}>
        {
          list.map((item, idx) => (
            <Block
              key={idx}
              style={styles.block}
              onPress={() => onPress(item)}
            >
              <View style={styles.subtitle}>
                <Text type={TextEnum.subtitle}>{item.name}</Text>
              </View>
              <Text type={TextEnum.description}>{item.description}</Text>
            </Block>
          ))
        }
      </ScrollView>
    </Container>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  block: {
    marginBottom: 10,
    marginHorizontal: 16,
  },
  title: {
    paddingTop: 10,
    paddingBottom: 14,
    marginHorizontal: 16,
  },
  subtitle: {
    marginBottom: 4
  }
})

export default Spinners;
