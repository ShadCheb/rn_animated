import React, { useMemo, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import Animated, {
  Easing,
  cancelAnimation,
  interpolate,
  useAnimatedProps,
  useAnimatedStyle,
  useSharedValue,
  withRepeat,
  withTiming
} from 'react-native-reanimated';
import Svg, { Circle, G } from 'react-native-svg';

import { Container } from '@components/common/Container';

const AnimatedSvg = Animated.createAnimatedComponent(Svg);
const AnimatedCircle = Animated.createAnimatedComponent(Circle);

enum SizeSpinner1Enum {
  large = 'large',
  small = 'small',
}

// params
const size = SizeSpinner1Enum.large;
const strokeWidth = 4;
const color = 'orange';

const Spinner1 = () => {
  const rotation = useSharedValue(0);
  const strokeDashoffset = useSharedValue(0);

  const data = useMemo(() => {
    const radius = size === 'large' ? 16 : 8;
    const halfCircle = radius + strokeWidth;
    const circleCircumference = 2 * Math.PI * radius;
    const duration = size === 'large' ? 3000 : 5000;

    return {
      radius,
      halfCircle,
      circleCircumference,
      duration
    };
  }, [size])

  const AnimatedStyles = {
    circle: useAnimatedStyle(() => {
      const rotationDegree = interpolate(rotation.value, [0, 1], [0, 360]);

      return {
        transform: [{ rotate: `${rotationDegree}deg` }]
      };
    })
  };

  const AnimatedProps = {
    art: useAnimatedProps(() => {
      return {
        strokeDashoffset: strokeDashoffset.value
      };
    })
  };

  useEffect(() => {
    const startAnimation = () => {
      rotation.value = 0;
      rotation.value = withRepeat(withTiming(1, { duration: data.duration, easing: Easing.linear }), -1, false);
      strokeDashoffset.value = (4 * data.circleCircumference) / 5;
      strokeDashoffset.value = withRepeat(
        withTiming(data.circleCircumference / 5, { duration: data.duration, easing: Easing.linear }),
        -1,
        true
      );
    };

    startAnimation();

    return () => {
      cancelAnimation(rotation);
    };
  }, []);

  return (
    <Container>
      <View style={styles.loader}>
        <AnimatedSvg
          width={data.radius * 2}
          height={data.radius * 2}
          viewBox={`0 0 ${data.halfCircle * 2} ${data.halfCircle * 2}`}
          style={AnimatedStyles.circle}
        >
          <G>
            <Circle
              cx="50%"
              cy="50%"
              stroke={color}
              strokeWidth={strokeWidth}
              r={data.radius}
              fill="transparent"
              strokeOpacity={0.2}
            />
            <AnimatedCircle
              animatedProps={AnimatedProps.art}
              cx="50%"
              cy="50%"
              stroke={color}
              strokeWidth={strokeWidth}
              r={data.radius}
              fill="transparent"
              strokeDasharray={data.circleCircumference}
              strokeDashoffset={strokeDashoffset.value}
              strokeLinecap="round"
            />
          </G>
        </AnimatedSvg>
      </View>
    </Container>
  );
}

const styles = StyleSheet.create({
  loader: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    zIndex: 1
  },
})

export default Spinner1;
