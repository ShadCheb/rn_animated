import React, { useState, useEffect } from 'react';
import { StyleSheet, View, FlatList, FlatListProps } from 'react-native';
import { Shadow, ShadowProps } from 'react-native-shadow-2';
import Animated, {
  SharedValue,
  interpolate,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue
} from 'react-native-reanimated';

import { Container } from '@components/common/Container';

import { OptionOther2, ICheckboxItem } from './OptionScroll3';

const AnimatedFlatlist = Animated.createAnimatedComponent<FlatListProps<ICheckboxItem>>(FlatList);

const ITEM_SIZE = 46;

const shadowConfigs: ShadowProps = {
  offset: [0, 8],
  distance: 32,
  startColor: 'rgba(231, 225, 216, 0.25)'
};

// Props
const list: ICheckboxItem[] = [
  {
    brief: 'option1',
    value: 'Option 1'
  },
  {
    brief: 'option2',
    value: 'Option 2'
  },
  {
    brief: 'option3',
    value: 'Option 3'
  },
  {
    brief: 'option4',
    value: 'Option 4'
  },
  {
    brief: 'option5',
    value: 'Option 5'
  }
];
const select = list[3];
const onSelectOption = (item: ICheckboxItem) => {
  // Выбираем элемент
}

const Other2 = () => {
  const currentItem = useSharedValue(0);

  const [options, setOptions] = useState<ICheckboxItem[]>([]);

  useEffect(() => {
    const optionsResult = [
      // Добавляем пустые элементы для отступов
      { brief: '', value: '' },
      ...list,
      { brief: '', value: '' },
      { brief: '', value: '' }
    ];

    setOptions(optionsResult);
  }, [list]);

  const scrollHandler = useAnimatedScrollHandler({
    onScroll: (event) => {
      currentItem.value = Math.round(event.contentOffset.y / ITEM_SIZE) + 1; // index активного элемента
    }
  });

  return (
    <Container style={styles.container}>
      <View style={styles.block}>
        <Shadow viewStyle={styles.topShadow} {...shadowConfigs}>
          <View />
        </Shadow>

        <AnimatedFlatlist
          style={styles.bodyList}
          scrollEventThrottle={1}
          showsVerticalScrollIndicator={false}
          onScroll={scrollHandler}
          snapToInterval={ITEM_SIZE}
          data={options}
          keyExtractor={item => item.brief}
          renderItem={({ item, index }) => (
            <OptionOther2
              index={index}
              item={item}
              currentItem={currentItem}
              select={select}
              onSelectOption={onSelectOption}
            />
          )}
        />

        <Shadow viewStyle={styles.bottomShadow} {...shadowConfigs}>
          <View />
        </Shadow>
      </View>

    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 120,
    backgroundColor: '#FBFAFA'
  },
  block: {
    height: 180,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: 'rgba(231, 225, 216, 0.3)',
    backgroundColor: '#fff',
    overflow: 'hidden',
  },
  topShadow: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0
  },
  bottomShadow: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0
  },


  body: {
    height: 200
  },
  bodyList: {
    height: '100%'
  },
})

export default Other2;
