import React from 'react';
import { StyleSheet, View, TouchableOpacity, Image } from 'react-native';
import Animated, { interpolate, SharedValue, useAnimatedStyle, withTiming } from 'react-native-reanimated';

import { UserIcon } from './icons/UserIcon';
import { Text, TextEnum } from 'components/ui/Text';

const ITEM_SIZE = 46;

export interface ICheckboxItem {
  brief: string;
  value: string;
  avatar?: string;
}

export const OptionOther2 = ({
  index,
  currentItem,
  item,
  select,
  onSelectOption
}: {
  index: number;
  currentItem: SharedValue<number>;
  item: ICheckboxItem;
  select?: ICheckboxItem | string;
  onSelectOption: (item: ICheckboxItem) => void;
}) => {
  const inputRange = [index - 1, index, index + 1];

  const isSelected =
    typeof select === 'string' ? item.brief === select : item.brief === select?.brief;

  const animatedContainerStyle = useAnimatedStyle(() => {
    const rotate = withTiming(interpolate(currentItem.value, inputRange, [100, 0, -100]));

    return {
      transform: [{ rotateX: `${rotate}deg` }],
      opacity: withTiming(interpolate(currentItem.value, inputRange, [0.55, 1, 0.55]))
    };
  });

  return (
    <TouchableOpacity
      activeOpacity={.9}
      disabled={!item.value && !item.brief}
      onPress={() => onSelectOption(item)}
    >
      <Animated.View
        style={[styles.item, isSelected && styles.itemSelect, animatedContainerStyle]}
      >
        {
          item.hasOwnProperty('avatar')
            ? (
              <View style={styles.avatar}>
                {
                  item.avatar
                    ? <Image style={styles.avatarImg} source={{ uri: item.avatar }} />
                    : <UserIcon style={styles.empty} color='#D0C7BB' />
                }
              </View>
            )
            : null
        }
        <Text
          type={TextEnum.text}
          numberOfLines={1}
        >
          {item.value}
        </Text>
      </Animated.View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  item: {
    paddingHorizontal: 14,
    flexDirection: 'row',
    alignItems: 'center',
    height: ITEM_SIZE,
  },
  itemSelect: {
    backgroundColor: '#F3E6DD',
    borderRadius: 8
  },
  avatar: {
    marginRight: 5,
    width: 20,
    height: 20,
    borderRadius: 10,
  },
  avatarImg: {
    width: '100%',
    height: '100%'
  },
  empty: {
    width: '56%',
    height: '56%'
  }
});
