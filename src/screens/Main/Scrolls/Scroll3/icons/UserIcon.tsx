import React from 'react';
import { ViewStyle } from 'react-native';
import Svg, { Path, SvgProps } from 'react-native-svg';

export type IUserIconProps = {
  style?: Partial<SvgProps & ViewStyle>;
  color?: string;
};

export const UserIcon = ({
  style,
  color = 'orange'
}: IUserIconProps) => {
  return (
    <Svg {...style} width={style?.width || 50} height={style?.height || 56} viewBox="0 0 50 56">
      <Path
        d="M10 15.4985C10 7.18183 16.6783 0.5 24.9986 0.5C33.3181 0.5 40 7.18109 40 15.4985C40 23.8185 33.3185 30.5 24.9986 30.5C16.6779 30.5 10 23.8178 10 15.4985Z"
        fill={color}
      />
      <Path
        d="M8.40575 34.8954C12.8942 33.4466 18.8446 33 24.9984 33C31.1157 33 37.0642 33.4314 41.5584 34.8623C43.8136 35.5803 45.8976 36.609 47.4452 38.1338C49.0594 39.7241 50 41.7728 50 44.2188C50 46.6594 49.0684 48.7094 47.463 50.3064C45.9226 51.8387 43.8456 52.8775 41.5925 53.6047C37.1033 55.0534 31.1522 55.5 24.9984 55.5C18.8814 55.5 12.9336 55.0694 8.44001 53.6389C6.18513 52.9211 4.10136 51.8923 2.55393 50.3672C0.939912 48.7764 0 46.7274 0 44.2812C0 41.8408 0.931199 39.7909 2.53621 38.1939C4.07632 36.6614 6.15295 35.6226 8.40575 34.8954Z"
        fill={color}
      />
    </Svg>
  );
};
