import React, { useCallback, useRef, useState, useEffect } from 'react';
import { KeyboardAvoidingView, LayoutChangeEvent, Platform, StyleSheet, View, ViewStyle } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Animated, {
  SharedValue,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue
} from 'react-native-reanimated';
import { Shadow, ShadowProps } from 'react-native-shadow-2';

const isAppleDevice = Platform.OS === 'ios';

const shadowConfigs: ShadowProps = {
  offset: [0, 8],
  distance: 32,
  startColor: 'rgba(231, 225, 216, 0.25)'
};

const AnimatedKeyboardAwareScrollView = Animated.createAnimatedComponent(KeyboardAwareScrollView);

export interface IHeaderAnimatedLayoutProps {
  refElem?: any;
  extraHeight?: number;
  style?: ViewStyle;
  children: JSX.Element;
  isTopScrolled?: SharedValue<boolean>;
}

export const AnimatedScrollArea = ({
  refElem,
  extraHeight,
  style,
  children,
  isTopScrolled
}: IHeaderAnimatedLayoutProps) => {
  const topShadow = useSharedValue(false);
  const bottomShadow = useSharedValue(false);

  const refElemBoottomContainer = useRef<View>(null);

  const [yBottomList, setYBottomList] = useState(0);
  const [yBottomContainer, setYBottomContainer] = useState(0);
  const [isAnimated, setIsAnimated] = useState(false);

  useEffect(() => {
    if (!(yBottomContainer + yBottomList)) return;

    if (yBottomContainer <= yBottomList) {
      setIsAnimated(true);
      bottomShadow.value = true;
    } else {
      setIsAnimated(false);
      bottomShadow.value = false;
    }
  }, [yBottomList, yBottomContainer])

  const AnimatedStyles = {
    top: useAnimatedStyle(() => {
      return {
        opacity: topShadow.value ? 1 : 0,
        zIndex: 5
      };
    }),
    bottom: useAnimatedStyle(() => {
      return {
        opacity: bottomShadow.value ? 1 : 0,
        zIndex: 5
      };
    })
  };

  const scrollHandler = useAnimatedScrollHandler({
    onScroll: (event) => {
      topShadow.value = event.contentOffset.y > 0;
      bottomShadow.value =
        event.contentSize.height - event.layoutMeasurement.height > event.contentOffset.y + 2;

      if (isAnimated && isTopScrolled) {
        const isChange = Boolean(event.contentOffset.y > 0);

        if (isChange !== isTopScrolled.value) {
          isTopScrolled.value = isChange;
        }
      }
    }
  });

  // Только для ios
  const setRefFormik = useCallback((e: any) => {
    if (refElem) {
      refElem.current = e;
    }
  }, []);

  const onLayoutBottomList = ({ nativeEvent }: LayoutChangeEvent) => {
    setYBottomList(nativeEvent.layout.height);
  }

  const onLayoutBottomContainer = () => {
    refElemBoottomContainer.current?.measure((fx, fy, w, h, px, py) => {
      setYBottomContainer(py);
    });
  }

  return (
    <View style={styles.container}>
      <Animated.View style={AnimatedStyles.top}>
        <Shadow viewStyle={styles.topShadow} {...shadowConfigs}>
          <View />
        </Shadow>
      </Animated.View>

      {isAppleDevice ? (
        <AnimatedKeyboardAwareScrollView
          innerRef={setRefFormik}
          enableOnAndroid
          extraHeight={extraHeight}
          style={styles.flex}
          onScroll={scrollHandler}
        >
          <View style={[styles.flex, style]} onLayout={onLayoutBottomList}>{children}</View>
        </AnimatedKeyboardAwareScrollView>
      ) : (
        <KeyboardAvoidingView style={styles.flex} keyboardVerticalOffset={extraHeight}>
          <Animated.ScrollView
            ref={refElem}
            style={styles.flex}
            onScroll={scrollHandler}
          >
            <View
              style={[styles.flex, style]}
              onLayout={onLayoutBottomList}
            >{children}</View>
          </Animated.ScrollView>
        </KeyboardAvoidingView>
      )}

      <Animated.View style={AnimatedStyles.bottom}>
        <Shadow viewStyle={styles.bottomShadow} {...shadowConfigs}>
          <View
            ref={refElemBoottomContainer}
            collapsable={false}
            onLayout={onLayoutBottomContainer}
          />
        </Shadow>
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FBFAFA',
    overflow: 'hidden'
  },
  flex: {
    flex: 1
  },
  topShadow: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 1
  },
  bottomShadow: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 0
  }
});
