import { useEffect, useState } from 'react';
import { Keyboard } from 'react-native';
import { SharedValue } from 'react-native-reanimated';

/**
 * Открыта ли клавиатура
 * @param initValue Значение при инициализации
 * @returns {Boolean}
 */
export const useShowKeyboard = (props?: {
  initValue?: boolean;
  heightKeyboard?: SharedValue<number>;
  // changeHeightKeyboard?: React.Dispatch<React.SetStateAction<number>>;
}) => {
  const { initValue = false, heightKeyboard } = props || {};

  const [isShowKeyboard, setIsShowKeyboard] = useState(initValue);

  useEffect(() => {
    const handlerKeyboardShow = Keyboard.addListener('keyboardDidShow', keybordDidShowHandler);
    const handlerKeyboardHide = Keyboard.addListener('keyboardDidHide', keybordDidHideHandler);

    return () => {
      handlerKeyboardShow?.remove();
      handlerKeyboardHide?.remove();
    };
  }, []);

  const keybordDidShowHandler = (e: any) => {
    if (heightKeyboard) {
      heightKeyboard.value = e.endCoordinates.height;
    }

    setIsShowKeyboard(true);
  };

  const keybordDidHideHandler = () => {
    if (heightKeyboard) {
      heightKeyboard.value = 0;
    }

    setIsShowKeyboard(false);
  };

  return isShowKeyboard;
};
