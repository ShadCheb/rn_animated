import React, { useState } from 'react';
import { TextStyle } from 'react-native';
import { useSharedValue } from 'react-native-reanimated';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import { AnimatedContainer } from './AnimatedContainer';
import { AnimatedHeader } from './AnimatedHeader';
import { AnimatedScreenTitle } from './AnimatedScreenTitle';
import { Scroll2Context } from './Scroll2Context';
import { Tab1Scroll2 } from './Tab1Scroll2';

const Tab = createMaterialTopTabNavigator();

const screenOptions = {
  tabBarStyle: {
    width: 'auto',
    borderBottomWidth: 1,
    borderColor: '#E8EBEF',
    backgroundColor: 'transparent',
    elevation: 0
  },
  tabBarLabelStyle: {
    fontSize: 18,
    lineHeight: 24,
    fontWeight: 'bold',
    textTransform: 'none'
  } as TextStyle,
  tabBarIndicatorStyle: {
    backgroundColor: '#AA7751'
  },
  tabBarItemStyle: { width: 'auto' },
  tabBarScrollEnabled: true,
  lazy: true,
  tabBarActiveTintColor: '#8E613F',
  tabBarInactiveTintColor: '#A9A197'
};

const tabs = [
  {
    value: 'Tab 1',
    component: Tab1Scroll2
  },
  {
    value: 'Tab 2',
    component: Tab1Scroll2
  },
];

const Scroll1 = () => {
  const [isShowHeaderScreen, setIsShowHeaderScreen] = useState(true);

  const heightKeyboard = useSharedValue(0);
  const isTopScrolled = useSharedValue(false);

  return (
    <Scroll2Context.Provider
      value={{
        isTopScrolled,
        heightKeyboard,
        showHeader: isShowHeaderScreen,
        changeShowHeader: setIsShowHeaderScreen
      }}
    >
      <AnimatedContainer
        isTopScrolled={isTopScrolled}
        edges={['top', 'bottom']}
      >
        {isShowHeaderScreen ? (
          <>
            <AnimatedHeader isTopScrolled={isTopScrolled} />
            <AnimatedScreenTitle
              isTopScrolled={isTopScrolled}
              title="Title"
            />
          </>
        ) : null}

        <Tab.Navigator
          screenOptions={{
            ...screenOptions,
            ...(!isShowHeaderScreen ? { tabBarStyle: { height: 0 } } : {})
          }}
        >
          {tabs.map((tab, index) => (
            <Tab.Screen
              key={`tab_${index}`}
              name={tab.value}
              component={tab.component}
            />
          ))}
        </Tab.Navigator>
      </AnimatedContainer>
    </Scroll2Context.Provider>
  );
}

export default Scroll1;
