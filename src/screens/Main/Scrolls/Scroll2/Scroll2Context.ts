import {createContext} from 'react';
import {SharedValue} from 'react-native-reanimated';

export const Scroll2Context = createContext<{
  isTopScrolled?: SharedValue<boolean>;
  heightKeyboard?: SharedValue<number>;
  showHeader: boolean;
  changeShowHeader: React.Dispatch<React.SetStateAction<boolean>>;
}>({
  isTopScrolled: undefined,
  heightKeyboard: undefined,
  showHeader: true,
  changeShowHeader: () => {},
});
