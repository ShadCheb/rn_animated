import React from 'react';
import { StyleProp, StyleSheet, ViewStyle } from 'react-native';
import Animated, { SharedValue, interpolateColor, useAnimatedStyle } from 'react-native-reanimated';
import { Edge, SafeAreaView } from 'react-native-safe-area-context';

interface IAnimatedContainerProps {
  children?: React.ReactNode;
  isTopScrolled: SharedValue<boolean>;
  style?: StyleProp<ViewStyle>;
  edges?: Edge[];
}

export const AnimatedContainer = ({
  children,
  isTopScrolled,
  style,
  edges,
}: IAnimatedContainerProps) => {
  const animatedBackgroundStyles = useAnimatedStyle(() => {
    return {
      backgroundColor: interpolateColor(
        Number(isTopScrolled.value),
        [0, 1],
        ['transparent', '#fff']
      )
    };
  });

  return (
    <Animated.View style={[styles.flex, animatedBackgroundStyles]}>
      <SafeAreaView style={[styles.flex, styles.background, style]} edges={edges}>
        {children}
      </SafeAreaView>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1
  },
  background: {
    backgroundColor: '#FBFAFA'
  }
});
