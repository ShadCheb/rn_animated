import React from 'react';
import { StyleSheet, View } from 'react-native';
import Animated, { SharedValue, useAnimatedStyle, withTiming } from 'react-native-reanimated';

import { Text, TextEnum } from '@components/ui/Text';

const transitionHeader = { duration: 300 };

export const AnimatedHeader = ({ isTopScrolled }: { isTopScrolled: SharedValue<boolean> }) => {
  const animatedHeaderStyles = useAnimatedStyle(() => {
    return {
      height: isTopScrolled.value
        ? withTiming(0, transitionHeader)
        : withTiming(78, transitionHeader), // 78 высота шапки
      overflow: 'hidden'
    };
  });

  return (
    <Animated.View style={animatedHeaderStyles}>
      <View style={styles.container}>
        <View style={styles.circle} />
        <Text type={TextEnum.title} numberOfLines={1}>
          Заголовок
        </Text>
      </View>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    paddingHorizontal: 2,
    paddingBottom: 24,
    marginHorizontal: 16,
    flexDirection: 'row',
    alignItems: 'center',
    zIndex: 5
  },
  circle: {
    marginRight: 10,
    width: 32,
    height: 32,
    borderRadius: 16,
    backgroundColor: '#F1EEE9',
  },
  homeIcon: {
    width: 24,
    height: 24
  }
});
