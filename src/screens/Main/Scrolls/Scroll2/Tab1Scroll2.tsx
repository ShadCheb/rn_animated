import React, { useCallback, useContext } from 'react';
import { StyleSheet, View } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { AnimatedScrollArea } from './AnimatedScrollArea';
import { Scroll2Context } from './Scroll2Context';

import { useScrollToTopAnimatedLayout } from './useScrollToTopAnimatedLayout';
import { useShowKeyboard } from './useShowKeyboard';

export const Tab1Scroll2 = () => {
  const { isTopScrolled, heightKeyboard, changeShowHeader } = useContext(Scroll2Context);

  // Определяем высоту клавиатуры для отступа
  useShowKeyboard({ heightKeyboard });
  // Прокрутка вверх
  const scrollRef = useScrollToTopAnimatedLayout(
    useCallback(() => {
      if (isTopScrolled) {
        isTopScrolled.value = false;
      }
    }, [])
  );

  const onPressInput = () => {
    changeShowHeader(false);
  };

  const onBlurInput = () => {
    changeShowHeader(true);
  };

  return (
    <AnimatedScrollArea
      refElem={scrollRef}
      isTopScrolled={isTopScrolled}
    >
      <View style={styles.container}>
        <View style={styles.shift} />

        <TextInput
          style={styles.input}
          onFocus={onPressInput}
          onBlur={onBlurInput}
        />

        <View style={styles.shift} />
      </View>
    </AnimatedScrollArea>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1
  },
  container: {
    flex: 1,
    marginHorizontal: 16,
  },
  shift: {
    height: 500
  },
  input: {
    padding: 10,
    marginBottom: 32,
    height: 40,
    borderWidth: 1,
    backgroundColor: '#fff'
  },
});
