import { useIsFocused } from '@react-navigation/native';
import { useEffect, useRef } from 'react';

/**
 * Прокрутка наверх при повторном фокусе в табах для headerAnimatedLayout
 * @param {Function} callback Действия, которые нужно выполнить после прокрутки наверх
 * @returns Возвращает ref для хранения элемента прокручиваемой области (например FlatList, ScrollList итд)
 */
export const useScrollToTopAnimatedLayout = (callback?: () => void) => {
  const scrollRef = useRef<any>(null);
  const isFirstLoad = useRef(true);

  const isFocused = useIsFocused();

  useEffect(() => {
    if (!isFocused) return;

    if (isFirstLoad.current) {
      isFirstLoad.current = false;

      return;
    }

    scrollRef.current?.scrollTo({
      y: 0,
      animated: true
    });

    callback?.();
  }, [isFocused]);

  return scrollRef;
};
