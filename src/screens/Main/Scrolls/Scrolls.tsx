import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';

import { Container } from '@components/common/Container';
import { Block } from '@components/ui/Block';

import { RouteNames } from '@constants/routeNames';
import { Text, TextEnum } from 'components/ui/Text';

type BlockType = {
  name: string
  navigateName: any
  description: string
}

const list: BlockType[] = [
  {
    name: 'Scroll1',
    navigateName: RouteNames.scroll1,
    description: 'Уменьшение заголовка экрана и изменение цвета шапки при прокрутке с затемнением контента',
  },
  {
    name: 'Scroll2',
    navigateName: RouteNames.scroll2,
    description: 'Уменьшение заголовка экрана и изменение цвета шапки при прокрутке при прокрутке с ' +
      'затемнением контента во вкладках. Корректное открытие клавиатуры',
  },
  {
    name: 'Scroll3',
    navigateName: RouteNames.scroll3,
    description: 'Выбор из списка в виде объемного цилиндра',
  },
];

const Scrolls = ({ navigation }: any) => {
  const onPress = (item: BlockType) => navigation.navigate(item.navigateName);

  return (
    <Container>
      <View style={styles.title}>
        <Text type={TextEnum.title}>Scrolls</Text>
      </View>
      <ScrollView style={styles.body}>
        {
          list.map((item, idx) => (
            <Block
              key={idx}
              style={styles.block}
              onPress={() => onPress(item)}
            >
              <View style={styles.subtitle}>
                <Text type={TextEnum.subtitle}>{item.name}</Text>
              </View>
              <Text type={TextEnum.description}>{item.description}</Text>
            </Block>
          ))
        }
      </ScrollView>
    </Container>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  block: {
    marginBottom: 10,
    marginHorizontal: 16,
  },
  title: {
    marginBottom: 5,
    paddingTop: 10,
    paddingBottom: 14,
    marginHorizontal: 16,
  },
  subtitle: {
    marginBottom: 4
  }
})

export default Scrolls;
