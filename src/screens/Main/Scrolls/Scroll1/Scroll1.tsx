import React from 'react';
import { useSharedValue } from 'react-native-reanimated';

import { AnimatedContainer } from './AnimatedContainer';
import { AnimatedHeader } from './AnimatedHeader';
import { AnimatedList } from './AnimatedList';
import { AnimatedScreenTitle } from './AnimatedScreenTitle';

const Scroll1 = () => {
  const isTopScrolled = useSharedValue(false);

  return (
    <AnimatedContainer
      isTopScrolled={isTopScrolled}
      edges={['top', 'bottom']}
    >
      <AnimatedHeader isTopScrolled={isTopScrolled} />

      <AnimatedScreenTitle
        isTopScrolled={isTopScrolled}
        title="Title screen"
      />
      <AnimatedList isTopScrolled={isTopScrolled} />
    </AnimatedContainer>
  );
};

export default Scroll1;
