import React, { useRef, useEffect, useState } from 'react';
import {
  StyleSheet,
  FlatList,
  View,
  FlatListProps,
} from 'react-native';
import Animated, {
  SharedValue,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import { Shadow, ShadowProps } from 'react-native-shadow-2';

import { Block } from '@components/ui/Block';
import { Text, TextEnum } from '@components/ui/Text';

const AnimatedFlatlist =
  Animated.createAnimatedComponent<
    FlatListProps<{ id: number; name: string }>
  >(FlatList);

const shadowConfigs: ShadowProps = {
  offset: [0, 0],
  distance: 32,
  startColor: 'rgba(231, 225, 216, 0.25)',
};

const data = [
  {
    id: 0,
    name: 'option 1',
  },
  {
    id: 1,
    name: 'option 2',
  },
  {
    id: 2,
    name: 'option 3',
  },
  {
    id: 3,
    name: 'option 4',
  },
  {
    id: 4,
    name: 'option 5',
  },
  {
    id: 5,
    name: 'option 6',
  },
  {
    id: 6,
    name: 'option 7',
  },
  {
    id: 7,
    name: 'option 8',
  },
];

export const AnimatedList = ({ isTopScrolled }: { isTopScrolled?: SharedValue<boolean> }) => {
  const topShadow = useSharedValue(false);
  const bottomShadow = useSharedValue(false);

  const refElemBoottomContainer = useRef<View>(null);

  const [yBottomList, setYBottomList] = useState(0);
  const [yBottomContainer, setYBottomContainer] = useState(0);
  const [isAnimated, setIsAnimated] = useState(false);

  useEffect(() => {
    if (!(yBottomContainer + yBottomList)) return;

    if (yBottomContainer <= yBottomList) {
      setIsAnimated(true);
      bottomShadow.value = true;
    } else {
      setIsAnimated(false);
      bottomShadow.value = false;
    }
  }, [yBottomList, yBottomContainer])

  const scrollHandler = useAnimatedScrollHandler({
    onScroll: (event) => {
      topShadow.value = event.contentOffset.y > 0;
      bottomShadow.value =
        event.contentSize.height - event.layoutMeasurement.height > event.contentOffset.y + 2;
      if (isTopScrolled && isAnimated) {
        isTopScrolled.value = event.contentOffset.y > 0;
      }
    }
  });

  const AnimatedStyles = {
    top: useAnimatedStyle(() => {
      return {
        opacity: topShadow.value ? 1 : 0
      };
    }),
    bottom: useAnimatedStyle(() => {
      return {
        opacity: bottomShadow.value ? 1 : 0
      };
    })
  };

  const onContentSizeChange = (w: number, h: number) => {
    setYBottomList(h);
  }

  const onLayoutBottomContainer = () => {
    refElemBoottomContainer.current?.measure((fx, fy, w, h, px, py) => {
      setYBottomContainer(py);
    });
  }

  return (
    <View style={styles.container}>
      <Animated.View style={AnimatedStyles.top}>
        <Shadow viewStyle={styles.topShadow} {...shadowConfigs}>
          <View />
        </Shadow>
      </Animated.View>

      <AnimatedFlatlist
        style={styles.list}
        data={data}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <>
            <Block
              style={styles.block}
            >
              <Text type={TextEnum.subtitle}>{item.name}</Text>
            </Block>
          </>
        )}
        onContentSizeChange={onContentSizeChange}
        scrollEventThrottle={0}
        onScroll={scrollHandler}
        onScrollToIndexFailed={() => { }}
      />

      <Animated.View style={AnimatedStyles.bottom}>
        <Shadow viewStyle={styles.bottomShadow} {...shadowConfigs}>
          <View
            ref={refElemBoottomContainer}
            collapsable={false}
            onLayout={onLayoutBottomContainer}
          />
        </Shadow>
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    overflow: 'hidden'
  },
  list: {
    paddingHorizontal: 16
  },
  block: {
    marginBottom: 8,
    height: 120
  },
  topShadow: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0
  },
  bottomView: {
    width: '100%',
    height: 0
  },
  bottomShadow: {
    position: 'absolute',
    bottom: 100,
    left: 0,
    right: 0,
    zIndex: 5
  },
});
