import React from 'react';
import { StyleProp, StyleSheet, TextStyle } from 'react-native';
import Animated, { SharedValue, useAnimatedStyle } from 'react-native-reanimated';

interface IAnimatedScreenTitleProps {
  isTopScrolled?: SharedValue<boolean>;
  title: string;
  style?: StyleProp<TextStyle>;
}

export const AnimatedScreenTitle = ({ title, style, isTopScrolled }: IAnimatedScreenTitleProps) => {
  const animatedTestStyle = useAnimatedStyle(() => {
    return isTopScrolled?.value
      ? {
        fontSize: 18,
        lineHeight: 24
      }
      : {
        fontSize: 24,
        lineHeight: 32
      };
  });

  return (
    <Animated.Text style={[styles.container, style, animatedTestStyle]}>{title}</Animated.Text>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    marginTop: 8,
    paddingBottom: 20,
    fontWeight: '700',
    color: '#312E29'
  }
});
