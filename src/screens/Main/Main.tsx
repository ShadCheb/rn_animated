import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';

import { Container } from '@components/common/Container';
import { Block } from '@components/ui/Block';

import { RouteNames } from '@constants/routeNames';
import { Text, TextEnum } from 'components/ui/Text';

type BlockType = {
  name: string
  navigateName: any
  description: string
}

const list: BlockType[] = [
  {
    name: 'Scrolls',
    navigateName: RouteNames.scrolls,
    description: 'Прокрутка экрана, списка (FlatList, ScrollView итд)',
  },
  {
    name: 'Spinners',
    navigateName: RouteNames.spinners,
    description: 'Индикатор загрузки, скачивания, ожидания',
  },
  {
    name: 'Gesture handler',
    navigateName: RouteNames.gesture,
    description: 'Обработка жестов, перемещения пальцем по экрану',
  },
  {
    name: 'Other',
    navigateName: RouteNames.other,
    description: 'Различная анимация',
  },
];

const Main = ({ navigation }: any) => {
  const onPress = (item: BlockType) => navigation.navigate(item.navigateName);

  return (
    <Container>
      <View style={styles.title}>
        <Text type={TextEnum.title}>Анимация RN</Text>
      </View>
      <ScrollView style={styles.body}>
        {
          list.map((item, idx) => (
            <Block
              key={idx}
              style={styles.block}
              onPress={() => onPress(item)}
            >
              <View style={styles.subtitle}>
                <Text type={TextEnum.subtitle}>{item.name}</Text>
              </View>
              <Text type={TextEnum.description}>{item.description}</Text>
            </Block>
          ))
        }
      </ScrollView>
    </Container>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  block: {
    marginBottom: 10,
    marginHorizontal: 16,
  },
  title: {
    paddingTop: 10,
    paddingBottom: 14,
    marginHorizontal: 16,
  },
  subtitle: {
    marginBottom: 4
  }
})

export default Main;
