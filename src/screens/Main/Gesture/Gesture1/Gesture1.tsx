import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import {
  GestureHandlerRootView,
  PanGestureHandler,
  PanGestureHandlerGestureEvent
} from 'react-native-gesture-handler';
import Animated, {
  interpolateColor,
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useDerivedValue,
  useSharedValue,
  withSpring
} from 'react-native-reanimated';

import { Container } from '@components/common/Container';
import { ReText } from 'components/ui/Retext';
import { MikeIcon } from './icons/MikeIcon';
import { ArrowUpIcon } from './icons/ArrowUpIcon';
import { CloseIcon } from './icons/CloseIcon';
import { MikeVoiceIcon } from './icons/MikeVoiceIcon';

enum RecordButtonVariant {
  START = 'start',
  RECORD = 'record',
  CANCEL = 'cancel',
  SEND = 'send'
}

type AnimatedGHContext = {
  completed: boolean;
};

const SIZE = 200;
const SIZE_BUTTON = 56;
const BORDER_WIDTH = 4;
const MAX_SHIFT = SIZE / 4 - BORDER_WIDTH;

const COLOR_WHITE = '#FFFFFF';
const COLOR_RED_LIGHT = '#FFB8A9';
const COLOR_RED = '#EA5419';
const COLOR_GREEN_LIGHT = '#C9E3D5';
const COLOR_GREEN = '#519F75';
const COLOR_RED_TEXT = '#EA5419';
const COLOR_GREEN_TEXT = '#519F75';
const COLOR_DISABLED = '#D0C7BB';

const tapAndSpeakText = 'Tap on the button';
const sendText = 'Send';
const upToSendText = 'Up to send';

const cancelText = 'Cancel';
const downToCancelText = 'Down to cancel';

const onSend = () => {

}
const onCancel = () => {

}

const Gesture1 = () => {
  const Y = useSharedValue(0);

  const [isStart, setIsStart] = useState(false);

  const currentState = useSharedValue(RecordButtonVariant.START);

  const upText = useDerivedValue(() => {
    return currentState.value === RecordButtonVariant.START
      ? tapAndSpeakText
      : currentState.value === RecordButtonVariant.SEND
        ? sendText
        : upToSendText;
  });

  const downText = useDerivedValue(() => {
    return currentState.value === RecordButtonVariant.START
      ? ''
      : currentState.value === RecordButtonVariant.CANCEL
        ? cancelText
        : downToCancelText;
  });

  const handleComplete = (value: number) => {
    if (value > 0) {
      onSend();
    } else {
      onCancel();
    }
  };

  const animatedGestureHandler = useAnimatedGestureHandler<
    PanGestureHandlerGestureEvent,
    AnimatedGHContext
  >({
    onActive: (e, ctx) => {
      const newValue = e.translationY;

      if (newValue > -MAX_SHIFT && newValue < MAX_SHIFT) {
        Y.value = newValue;
        if (currentState.value !== RecordButtonVariant.RECORD) {
          currentState.value = RecordButtonVariant.RECORD;
        }
      } else if (newValue <= -MAX_SHIFT) {
        Y.value = -MAX_SHIFT;
        if (currentState.value !== RecordButtonVariant.SEND) {
          currentState.value = RecordButtonVariant.SEND;
        }
      } else {
        Y.value = MAX_SHIFT;
        if (currentState.value !== RecordButtonVariant.CANCEL) {
          currentState.value = RecordButtonVariant.CANCEL;
        }
      }
    },
    onEnd: () => {
      if (Y.value <= -MAX_SHIFT) {
        runOnJS(handleComplete)(1);
      } else if (Y.value >= MAX_SHIFT) {
        runOnJS(handleComplete)(-1);
      }
      Y.value = withSpring(0);
    }
  });

  const AnimatedStyles = {
    area: useAnimatedStyle(() => {
      return {
        borderColor: interpolateColor(
          Y.value,
          [-MAX_SHIFT, 0, MAX_SHIFT],
          [COLOR_GREEN_LIGHT, COLOR_GREEN_LIGHT, COLOR_RED_LIGHT]
        ),
        shadowColor:
          currentState.value === RecordButtonVariant.START
            ? 'transparent'
            : interpolateColor(
              Y.value,
              [-MAX_SHIFT, 0, MAX_SHIFT],
              [COLOR_GREEN_LIGHT, COLOR_GREEN_LIGHT, COLOR_RED_LIGHT]
            )
      };
    }),
    swipeable: useAnimatedStyle(() => {
      return {
        backgroundColor: interpolateColor(
          Y.value,
          [-MAX_SHIFT, 0, MAX_SHIFT],
          [COLOR_GREEN, COLOR_GREEN, COLOR_RED]
        ),
        transform: [{ translateY: Y.value }],
        shadowColor:
          currentState.value === RecordButtonVariant.START
            ? 'transparent'
            : interpolateColor(
              Y.value,
              [-MAX_SHIFT, 0, MAX_SHIFT],
              [COLOR_GREEN_LIGHT, COLOR_GREEN_LIGHT, COLOR_RED_LIGHT]
            )
      };
    }),
    textSuccess: useAnimatedStyle(() => ({
      color:
        currentState.value === RecordButtonVariant.SEND
          ? COLOR_GREEN_TEXT
          : COLOR_DISABLED
    })),
    textError: useAnimatedStyle(() => ({
      color:
        currentState.value === RecordButtonVariant.CANCEL
          ? COLOR_RED_TEXT
          : COLOR_DISABLED
    })),
    arrowUpIntercom: useAnimatedStyle(() => ({
      opacity: currentState.value === RecordButtonVariant.SEND ? 1 : 0
    })),
    closeIntercom: useAnimatedStyle(() => ({
      opacity: currentState.value === RecordButtonVariant.CANCEL ? 1 : 0
    })),
    mikeVoiceIntercom: useAnimatedStyle(() => ({
      opacity:
        currentState.value === RecordButtonVariant.SEND ||
          currentState.value === RecordButtonVariant.CANCEL
          ? 0
          : 1
    }))
  };

  const onStartRecord = () => {
    setIsStart(true);
    currentState.value = RecordButtonVariant.RECORD;
  };

  return (
    <Container>
      <View style={styles.body}>
        <ReText style={AnimatedStyles.textSuccess} text={upText} />
        <Animated.View style={[styles.area, AnimatedStyles.area]}>
          {!isStart ? (
            <TouchableOpacity
              activeOpacity={0.7}
              style={[styles.swipeable, styles.swipeableStart]}
              onPress={onStartRecord}
            >
              <MikeIcon style={styles.mikeVoiceIntercomIcon} color={COLOR_WHITE} />
            </TouchableOpacity>
          ) : (
            <GestureHandlerRootView style={styles.flex}>
              <PanGestureHandler onGestureEvent={animatedGestureHandler}>
                <Animated.View style={[styles.swipeable, AnimatedStyles.swipeable]}>
                  <Animated.View style={[styles.arrowUpIntercom, AnimatedStyles.arrowUpIntercom]}>
                    <ArrowUpIcon
                      style={styles.arrowUpIntercomIcon}
                      color={COLOR_WHITE}
                    />
                  </Animated.View>
                  <Animated.View style={[styles.closeIntercom, AnimatedStyles.closeIntercom]}>
                    <CloseIcon
                      style={styles.closeIntercomIcon}
                      color={COLOR_WHITE}
                    />
                  </Animated.View>
                  <Animated.View style={[styles.mikeVoiceIntercom, AnimatedStyles.mikeVoiceIntercom]}>
                    <MikeVoiceIcon
                      style={styles.mikeVoiceIntercomIcon}
                      color={COLOR_WHITE}
                    />
                  </Animated.View>
                </Animated.View>
              </PanGestureHandler>
            </GestureHandlerRootView>
          )}
        </Animated.View>

        <ReText style={AnimatedStyles.textError} text={downText} />
      </View>
    </Container>
  );
}

const styles = StyleSheet.create({
  body: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  flex: {
    flex: 1
  },
  area: {
    marginVertical: 16,
    width: SIZE,
    height: SIZE,
    borderRadius: SIZE / 2,
    borderWidth: BORDER_WIDTH,
    borderColor: COLOR_GREEN,
    backgroundColor: COLOR_WHITE,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: 0.1,
    shadowRadius: 6.12,
    elevation: 4
  },
  swipeable: {
    position: 'absolute',
    top: SIZE / 2 - SIZE_BUTTON / 2 - BORDER_WIDTH,
    left: SIZE / 2 - SIZE_BUTTON / 2 - BORDER_WIDTH,
    alignItems: 'center',
    justifyContent: 'center',
    height: SIZE_BUTTON,
    width: SIZE_BUTTON,
    borderRadius: SIZE_BUTTON / 2,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: 0.1,
    shadowRadius: 6.12,
    elevation: 4,
    zIndex: 3
  },
  swipeableStart: {
    backgroundColor: COLOR_GREEN,
    shadowColor: 'transparent'
  },
  mikeVoiceIntercom: {
    position: 'absolute',
    top: (SIZE_BUTTON - 24) / 2,
    left: (SIZE_BUTTON - 25) / 2
  },
  arrowUpIntercom: {
    position: 'absolute',
    top: (SIZE_BUTTON - 20) / 2,
    left: (SIZE_BUTTON - 21) / 2
  },
  closeIntercom: {
    position: 'absolute',
    top: (SIZE_BUTTON - 16) / 2,
    left: (SIZE_BUTTON - 17) / 2
  },
  mikeVoiceIntercomIcon: {
    width: 25,
    height: 24
  },
  arrowUpIntercomIcon: {
    width: 21,
    height: 20
  },
  closeIntercomIcon: {
    width: 17,
    height: 16
  },

  loader: {
    backgroundColor: 'rgba(255,255,255,.5)',
    borderRadius: SIZE_BUTTON / 2,
    zIndex: 5
  }
})

export default Gesture1;
