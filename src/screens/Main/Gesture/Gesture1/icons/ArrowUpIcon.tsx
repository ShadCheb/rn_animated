import React from 'react';
import { ViewStyle } from 'react-native';
import Svg, { Path, SvgProps } from 'react-native-svg';

export type IArrowUpIconProps = {
  style?: Partial<SvgProps & ViewStyle>;
  color?: string;
};

export const ArrowUpIcon = ({
  style,
  color = 'orange'
}: IArrowUpIconProps) => {
  return (
    <Svg
      {...style}
      width={style?.width || 21}
      height={style?.height || 20}
      viewBox={style?.viewBox || '0 0 21 20'}
      fill={style?.fill || 'none'}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.50004 3.82843L1.91421 11.1213L0.5 9.70709L9.79294 0.707105L10.5 0L11.2071 0.707104L20.5 9.70709L19.0858 11.1213L11.5 3.82842V20H9.50004V3.82843Z"
        fill={color}
      />
    </Svg>
  );
};
