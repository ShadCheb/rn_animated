import React from 'react';
import { ViewStyle } from 'react-native';
import Svg, { Path, SvgProps } from 'react-native-svg';

export type ICloseIconProps = {
  style?: Partial<SvgProps & ViewStyle>;
  color?: string;
};

export const CloseIcon = ({
  style,
  color = 'orange'
}: ICloseIconProps) => {
  return (
    <Svg
      {...style}
      width={style?.width || 17}
      height={style?.height || 16}
      viewBox={style?.viewBox || '0 0 17 16'}
      fill={style?.fill || 'none'}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1.95455 0L0.500007 1.45454L7.04546 8L0.5 14.5455L1.95454 16L8.5 9.45455L15.0454 16L16.5 14.5454L9.95454 8L16.5 1.45457L15.0454 2.40811e-05L8.5 6.54546L1.95455 0Z"
        fill={color}
      />
    </Svg>
  );
};
