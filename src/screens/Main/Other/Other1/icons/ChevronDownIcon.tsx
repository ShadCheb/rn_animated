import React from 'react';
import { ViewStyle } from 'react-native';
import Svg, { Path, SvgProps } from 'react-native-svg';

export type IChevronDownIconProps = {
  style?: Partial<SvgProps & ViewStyle>;
  color?: string;
};

export const ChevronDownIcon = ({
  style,
  color = 'orange'
}: IChevronDownIconProps) => {
  return (
    <Svg
      {...style}
      width={style?.width || 16}
      height={style?.height || 16}
      viewBox={style?.viewBox || '0 0 16 16'}
      fill={style?.fill || 'none'}
    >
      <Path
        d="M5 6.5L8 9.5L11 6.5"
        stroke={color}
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};
