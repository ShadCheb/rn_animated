import React, { useRef } from 'react';
import { StyleSheet, View } from 'react-native';
import {
  Transition,
  Transitioning,
  TransitioningView,
} from 'react-native-reanimated';

import { Container } from '@components/common/Container';

import { BlockOther1 } from './BlockOther1';

const transition = (
  <Transition.Together>
    <Transition.In type="fade" durationMs={300} />
    <Transition.Change />
    <Transition.Out type="fade" durationMs={300} />
  </Transition.Together>
);
const data = [
  {
    id: 0,
    title: 'Title 1',
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Minus, ad, ' +
      'reiciendis quam aliquam nesciunt mollitia maiores voluptate sequi' +
      'exercitationem veniam reprehenderit consequuntur blanditiis et' +
      'accusamus. Doloremque reiciendis provident esse quo!'
  },
  {
    id: 1,
    title: 'Title 2',
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Minus, ad, ' +
      'reiciendis quam aliquam nesciunt mollitia maiores voluptate sequi' +
      'exercitationem veniam reprehenderit consequuntur blanditiis et' +
      'accusamus. Doloremque reiciendis provident esse quo!'
  }
]

const Other1 = () => {
  const refTransitionView = useRef<TransitioningView | null>(null);

  return (
    <Container style={styles.container}>
      <View style={styles.area}>
        <Transitioning.View ref={refTransitionView} transition={transition}>
          {
            data.map(item => (
              <BlockOther1
                key={item.id}
                item={item}
                refTransitionView={refTransitionView}
                style={styles.block}
              />
            ))
          }
        </Transitioning.View>
      </View>
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FBFAFA',
  },
  area: {
    flex: 1,
    paddingTop: 32,
    paddingHorizontal: 16,
  },
  block: {
    marginBottom: 10,
  }
})

export default Other1;
