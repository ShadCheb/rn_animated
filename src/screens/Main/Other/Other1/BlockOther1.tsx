import React, { useState, MutableRefObject } from 'react';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  StyleProp,
  ViewStyle
} from 'react-native';
import { Shadow, ShadowProps } from 'react-native-shadow-2';
import Animated, {
  useAnimatedStyle,
  TransitioningView,
  useSharedValue,
  withTiming
} from 'react-native-reanimated';

import { Text, TextEnum } from '@components/ui/Text';
import { Block } from '@components/ui/Block';

import { ChevronDownIcon } from './icons/ChevronDownIcon';

const shadowConfigs: ShadowProps = {
  offset: [0, 1],
  distance: 8,
  startColor: 'rgba(231, 225, 216, 0.22)'
};

const SIZE = 32;

interface IHeaderProps {
  style?: StyleProp<ViewStyle>;
  item: {
    id: number;
    title: string;
    text: string;
  },
  refTransitionView: MutableRefObject<TransitioningView | null>
}

export const BlockOther1 = ({
  style,
  item,
  refTransitionView
}: IHeaderProps) => {
  const [isVisible, setVisible] = useState(false);
  const rotation = useSharedValue(0);

  const rotationStyles = useAnimatedStyle(() => {
    return {
      transform: [{ rotate: `${rotation.value}deg` }]
    };
  });

  const onToggleExpand = () => {
    const valueResult = !rotation.value;

    refTransitionView.current?.animateNextTransition();

    setVisible(valueResult);
    rotation.value = withTiming(valueResult ? 180 : 0, { duration: 300 });
  };

  return (
    <Block style={[styles.block, style]}>
      <TouchableWithoutFeedback onPress={onToggleExpand}>
        <View style={styles.container}>
          <View style={styles.title}>
            <Text type={TextEnum.subtitle}>
              {item.title}
            </Text>
          </View>

          <Shadow {...shadowConfigs}>
            <View style={styles.circle}>
              <Animated.View style={rotationStyles}>
                <ChevronDownIcon style={styles.icon} color="orange" />
              </Animated.View>
            </View>
          </Shadow>
        </View>
      </TouchableWithoutFeedback>
      {isVisible && (
        <View style={styles.text}>
          <Text type={TextEnum.text}>
            {item.text}
          </Text>
        </View>
      )}
    </Block>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  block: {
    backgroundColor: '#fff'
  },
  text: {
    paddingVertical: 10,
    paddingHorizontal: 14,
    marginTop: 10,
    backgroundColor: '#FBFAFA',
    borderRadius: 4
  },
  circle: {
    height: SIZE,
    width: SIZE,
    borderRadius: SIZE / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  icon: {
    width: SIZE - 10,
    height: SIZE - 10
  }
})
