import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { RouteNames } from '@constants/routeNames';

import Main from '@screens/Main';

import Scrolls from '@screens/Main/Scrolls';
import Scroll1 from '@screens/Main/Scrolls/Scroll1';
import Scroll2 from '@screens/Main/Scrolls/Scroll2';
import Scroll3 from '@screens/Main/Scrolls/Scroll3';

import Spinners from '@screens/Main/Spinners';
import Spinner1 from '@screens/Main/Spinners/Spinner1';

import Gesture from '@screens/Main/Gesture';
import Gesture1 from '@screens/Main/Gesture/Gesture1';

import Other from '@screens/Main/Other';
import Other1 from '@screens/Main/Other/Other1';

const Stack = createStackNavigator();

const RootNavigator = () => (
  <NavigationContainer>
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name={RouteNames.main} component={Main} />

      <Stack.Screen name={RouteNames.scrolls} component={Scrolls} />
      <Stack.Screen name={RouteNames.scroll1} component={Scroll1} />
      <Stack.Screen name={RouteNames.scroll2} component={Scroll2} />
      <Stack.Screen name={RouteNames.scroll3} component={Scroll3} />

      <Stack.Screen name={RouteNames.spinners} component={Spinners} />
      <Stack.Screen name={RouteNames.spinner1} component={Spinner1} />

      <Stack.Screen name={RouteNames.gesture} component={Gesture} />
      <Stack.Screen name={RouteNames.gesture1} component={Gesture1} />

      <Stack.Screen name={RouteNames.other} component={Other} />
      <Stack.Screen name={RouteNames.other1} component={Other1} />
    </Stack.Navigator>
  </NavigationContainer>
);

export default RootNavigator;