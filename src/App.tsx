import React from 'react';
import { LogBox } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import { RootNavigator } from './navigation';

LogBox.ignoreAllLogs();

export const App = () => (
  <SafeAreaProvider>
    <RootNavigator />
  </SafeAreaProvider>
);
