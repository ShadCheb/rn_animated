export enum RouteNames {
  main = "Main",

  scrolls = "Scrolls",
  scroll1 = "Scroll1",
  scroll2 = "Scroll2",
  scroll3 = "Scroll3",

  spinners = "Spinners",
  spinner1 = "Spinner1",

  gesture = "Gesture",
  gesture1 = "Gesture1",

  other = "Other",
  other1 = "Other1",
}
