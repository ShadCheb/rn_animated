import React from 'react';
import { StyleSheet, StyleProp, ViewStyle, TouchableOpacity } from 'react-native';

interface IBlockProps {
  children?: React.ReactNode;
  style?: StyleProp<ViewStyle>;
  onPress?: () => void;
}

export const Block = (props: IBlockProps) => {
  const {
    children,
    style,
    onPress,
  } = props;
  return (
    <TouchableOpacity
      activeOpacity={.7}
      disabled={!Boolean(onPress)}
      style={[styles.container, style]}
      onPress={onPress}
    >
      {children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    paddingHorizontal: 14,
    borderWidth: 1,
    borderColor: '#e9e9e9',
    borderRadius: 8,
  }
});
