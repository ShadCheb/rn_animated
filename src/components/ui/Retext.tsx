import React from 'react';
import { StyleSheet, TextInput } from 'react-native';
import Animated, { useAnimatedProps } from 'react-native-reanimated';

const styles = StyleSheet.create({
  baseStyle: {
    color: 'black'
  }
});
Animated.addWhitelistedNativeProps({ text: true });

interface IReTextProps {
  text: Animated.SharedValue<string>;
  style?: Animated.AnimateProps<Record<string, unknown>>['style'];
}

const AnimatedTextInput = Animated.createAnimatedComponent(TextInput);

export const ReText = (props: IReTextProps) => {
  const { text, style } = { style: {}, ...props };
  const animatedProps = useAnimatedProps(() => {
    return {
      text: text.value
    } as any;
  });

  return (
    <AnimatedTextInput
      underlineColorAndroid="transparent"
      editable={false}
      value={text.value}
      style={[styles.baseStyle, style]}
      {...{ animatedProps }}
    />
  );
};
