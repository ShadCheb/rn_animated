import React from 'react';
import { StyleSheet, Text as RNText, TextProps, StyleProp, TextStyle } from 'react-native';

export enum TextEnum {
  title = 'title',
  subtitle = 'subtitle',
  text = 'text',
  description = 'description',
}

interface ITextProps extends TextProps {
  type: TextEnum;
  children?: string | number | React.ReactNode[];
  align?: 'auto' | 'left' | 'center' | 'justify';
  color?: string;
  numberOfLines?: number;
}

export const Text = (props: ITextProps) => {
  const {
    type,
    color = '#282828',
    align = 'auto',
    children,
    numberOfLines = 0,
  } = props;

  return (
    <RNText
      style={[
        styles[type],
        {
          color: color,
          textAlign: align,
        }
      ]}
      numberOfLines={numberOfLines}>
      {children}
    </RNText>
  );
};

const styles = StyleSheet.create({
  [TextEnum.title]: {
    fontSize: 20,
    lineHeight: 34,
    letterSpacing: 0.36,
    fontWeight: '700',
  },
  [TextEnum.subtitle]: {
    fontSize: 17,
    lineHeight: 22,
    letterSpacing: 0.36,
    fontWeight: '700',
  },
  [TextEnum.text]: {
    fontSize: 14,
    lineHeight: 18,
    fontWeight: '400',
  },
  [TextEnum.description]: {
    fontSize: 12,
    lineHeight: 16,
    fontWeight: '400',
    opacity: 0.7,
  },
});
