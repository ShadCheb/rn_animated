import React from 'react';
import { StyleProp, StyleSheet, ViewStyle } from 'react-native';
import { Edge, SafeAreaView } from 'react-native-safe-area-context';

interface IContainerProps {
  children?: React.ReactNode;
  edges?: Edge[];
  style?: StyleProp<ViewStyle>;
}

export const Container = ({
  children,
  edges = ['top', 'bottom'],
  style,
}: IContainerProps) => (
  <SafeAreaView style={[styles.area, style]} edges={edges}>
    {children}
  </SafeAreaView>
);

const styles = StyleSheet.create({
  area: {
    flex: 1,
    backgroundColor: 'white'
  },
});
