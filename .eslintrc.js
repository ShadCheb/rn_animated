module.exports = {
  root: true,
  parserOptions: {
    project: './jsconfig.json',
  },
  parser: '@typescript-eslint/parser',
  extends: [
    '@react-native-community',
    'plugin:@typescript-eslint/recommended',
  ],
  plugins: ['react-hooks', 'jest'],
  rules: {
    'jsx-quotes': [1, 'prefer-double'],
  },
  printWidth: 70,
  ignorePatterns: ['/*.*'],
};
